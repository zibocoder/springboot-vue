package com.zibocoder.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 菜单实体类
 *
 * @Author zibocoder
 * @Date 2024/11/27 21:13
 */
@Data
@TableName("sys_menu")
public class SysMenu implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 菜单ID
     */
    @TableId(type = IdType.AUTO)
    private Long menuId;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 父菜单ID
     */
    private Long parentId;
    /**
     * 显示顺序
     */
    private Integer menuSort;
    /**
     * 菜单类型（0目录 1菜单 2按钮）
     */
    private Integer menuType;
    /**
     * 路由地址
     */
    private String path;
    /**
     * 组件路径
     */
    private String component;
    /**
     * 权限标识
     */
    private String perms;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 状态：false-0不可用 true-1可用
     */
    private Boolean status;
    /**
     * 是否缓存（false-0不缓存 true-1缓存）
     */
    private Boolean cacheFlag;
    /**
     * 是否为外链（false-0否 true-1是）
     */
    private Boolean frameFlag;
    /**
     * 外链地址
     */
    private String frameUrl;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
