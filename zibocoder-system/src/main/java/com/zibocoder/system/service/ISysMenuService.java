package com.zibocoder.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zibocoder.system.domain.entity.SysMenu;

/**
 * 菜单管理 服务接口
 *
 * @Author zibocoder
 * @Date 2024/11/27 22:08
 */
public interface ISysMenuService extends IService<SysMenu> {
}
