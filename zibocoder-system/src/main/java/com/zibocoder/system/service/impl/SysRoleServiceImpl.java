package com.zibocoder.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zibocoder.system.domain.entity.SysMenu;
import com.zibocoder.system.domain.entity.SysRole;
import com.zibocoder.system.mapper.SysMenuMapper;
import com.zibocoder.system.mapper.SysRoleMapper;
import com.zibocoder.system.service.ISysMenuService;
import com.zibocoder.system.service.ISysRoleService;
import org.springframework.stereotype.Service;

/**
 * 角色 业务实现类
 *
 * @Author zibocoder
 * @Date 2024/11/27 22:16
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
}
