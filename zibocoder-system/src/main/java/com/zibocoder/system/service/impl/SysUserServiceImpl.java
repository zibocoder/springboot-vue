package com.zibocoder.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zibocoder.system.domain.entity.SysUser;
import com.zibocoder.system.mapper.SysUserMapper;
import com.zibocoder.system.service.ISysUserService;
import org.springframework.stereotype.Service;

/**
 * 用户 业务实现类
 *
 * @Author zibocoder
 * @Date 2024/11/27 22:16
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {
}
