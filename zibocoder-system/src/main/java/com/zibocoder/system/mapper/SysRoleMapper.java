package com.zibocoder.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zibocoder.system.domain.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色管理 Mapper
 * @Author zibocoder
 * @Date 2024/11/27 21:55
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
}
