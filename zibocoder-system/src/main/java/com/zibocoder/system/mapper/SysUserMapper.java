package com.zibocoder.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zibocoder.system.domain.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户管理 Mapper 接口
 *
 * @Author zibocoder
 * @Date 2024/11/27 21:50
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
}
