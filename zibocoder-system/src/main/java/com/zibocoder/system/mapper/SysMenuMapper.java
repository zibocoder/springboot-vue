package com.zibocoder.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zibocoder.system.domain.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 菜单管理 Mapper 接口
 *
 * @Author zibocoder
 * @Date 2024/11/27 21:51
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {
}
