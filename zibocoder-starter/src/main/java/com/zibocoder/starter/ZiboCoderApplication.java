package com.zibocoder.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZiboCoderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZiboCoderApplication.class, args);
    }
}
